function love.keypressed(key)
  if key == 'escape' then love.event.quit()
  elseif key == 'up' then love.joystickpressed(0, 4)
  elseif key == 'down' then love.joystickpressed(0, 5)
  elseif key == 'left' then love.joystickpressed(0, 6)
  elseif key == 'right' then love.joystickpressed(0, 7)
  elseif key == 'kp8' then love.joystickpressed(1, 4)
  elseif key == 'kp2' then love.joystickpressed(1, 5)
  elseif key == 'kp4' then love.joystickpressed(1, 6)
  elseif key == 'kp6' then love.joystickpressed(1, 7)
  end
end

function love.keyreleased(key)
  if key == 'escape' then love.event.quit()
  elseif key == 'up' then love.joystickreleased(0, 4)
  elseif key == 'down' then love.joystickreleased(0, 5)
  elseif key == 'left' then love.joystickreleased(0, 6)
  elseif key == 'right' then love.joystickreleased(0, 7)
  elseif key == 'kp8' then love.joystickreleased(1, 4)
  elseif key == 'kp2' then love.joystickreleased(1, 5)
  elseif key == 'kp4' then love.joystickreleased(1, 6)
  elseif key == 'kp6' then love.joystickreleased(1, 7)
  end
end
