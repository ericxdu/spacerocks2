--Space Rocks 2 - an asteroid-dodging game
--Copyright 2019, 2020, 2021 Eric Duhamel

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <https://www.gnu.org/licenses/>.

function lutro.load()
  math.randomseed(os.time())
  lutro.graphics.setDefaultFilter('nearest', 'nearest')
  LEFT, TOP, RIGHT, BOTTOM = 0, 0, 320, 240, 320, 240
  FONT = lutro.graphics.newImageFont("dos_8x8.png", "0123456789UDRL")
  BACKDROP = lutro.graphics.newImage("space02.png")
  COIN_SND = lutro.audio.newSource("coin00.wav", "static")
  LIFE_SND = lutro.audio.newSource("life.wav", "static")
  DEATH_SND = lutro.audio.newSource("death.wav", "static")
  THRUST_SND = lutro.audio.newSource("thrust01.wav", 'static')
  LOOP_0 = lutro.audio.newSource("loop_through_space.ogg", "stream")
  LOOP_0:setLooping(true)
  SCORE = 0
  THRUST, TX, TY = 75, 0, 0
  COINS, ROCKS, SHIPS = {}, {}, {}
  for i=1,4,1 do
    local rock = new_rock(i > 2)
    table.insert(ROCKS, rock)
  end
end

function lutro.update(dt)
  for i,obj in ipairs(SHIPS) do
    object_move(obj, dt)
    if obj.dead then
      if obj.y > BOTTOM or obj.y+obj.h < TOP then
        obj = false
        table.remove(SHIPS, i)
      end
    else
      obj_accelerate(obj, dt)
      local qx = 0
      if obj.aup then qx = qx+64 end
      if obj.aright then qx = qx+128 end
      if obj.adown then qx = qx+16 end
      if obj.aleft then qx = qx+32 end
      obj.q:setViewport(qx, 0, obj.w, obj.h)
      object_wrap_in(obj)
      if #COINS < 2 and math.random(1000) > 995 then
        local coin = new_coin()
        table.insert(COINS, coin)
      end
      obj_decelerate(obj, dt)
    end
  end
  for i,rock in ipairs(ROCKS) do
    object_move(rock, dt)
    object_wrap_in(rock)
    local ship = SHIPS[1]
    if ship and is_inside_box(ship.x, ship.y, LEFT, TOP, RIGHT, BOTTOM)
            and not ship.dead then
      if not is_outside(ship, rock, 7) then
        LOOP_0:stop()
        DEATH_SND:play()
        ship.dead = true
        ship.dx, ship.dy = rock.dx, rock.dy
        ship.q:setViewport(0, 0, ship.w, ship.h)
      end
    end
  end
  for i,coin in ipairs(COINS) do
    local frame = math.floor(lutro.timer.getTime()*15%8)*10
    coin.q:setViewport(frame, 0, coin.w, coin.h)
    if coin.dx > 50 then coin.dx=coin.dx-(50*dt)
    elseif coin.dx < -50 then coin.dx=coin.dx+(50*dt) end
    if coin.dy > 50 then coin.dy=coin.dy-(50*dt)
    elseif coin.dy < -50 then coin.dy=coin.dy+(50*dt) end
    object_move(coin, dt)
    local ship = SHIPS[1]
    if not is_inside_box(coin.x, coin.y, LEFT, TOP, RIGHT, BOTTOM) then
      if not ship or ship.dead then table.remove(COINS, i) end
    end
    object_wrap_in(coin)
    if ship and not ship.dead then
      if not is_outside(coin, ship, 6) then
        COIN_SND:stop()
        COIN_SND:play()
        table.remove(COINS, i)
        SCORE = SCORE + 50
      end
    end
  end
end

function lutro.draw()
  lutro.graphics.scale(2)
  lutro.graphics.draw(BACKDROP)  -- draw backdrop
  for i,ship in ipairs(SHIPS) do obj_draw(ship) end
  for i,coin in ipairs(COINS) do obj_draw(coin) end
  for i,rock in ipairs(ROCKS) do obj_draw(rock) end
  if #SHIPS<1 and math.floor(lutro.timer.getTime()*12%5)>2.5 then
    lutro.graphics.print("R", 1, 112)
    lutro.graphics.print("L", 312, 112)
  end
  lutro.graphics.setFont(FONT)
  local x = (RIGHT/2)
  lutro.graphics.print(SCORE, x, TOP) -- draw score
end

function lutro.joystickpressed(n, b)
  if #SHIPS < 1 then
    local ship = false
    if b == 6 then
      ship = new_obj("plus_ship.png", 336, 112, 16, 16)
      ship.dx = -15
    elseif b == 7 then
      ship = new_obj("plus_ship.png", -16, 112, 16, 16)
      ship.dx = 15
    end
    if ship then
      table.insert(SHIPS, ship)
      LOOP_0:play()
    end
  end
  if #SHIPS > n and not SHIPS[n+1].dead then
    local obj = SHIPS[n+1]
    if b == 4 then obj.aup = 75
    elseif b == 5 then obj.adown = 75
    elseif b == 6 then obj.aleft = 75
    elseif b == 7 then obj.aright = 75 end
    if obj.aup or obj.aright or obj.adown or obj.aleft then
      THRUST_SND:stop()
      THRUST_SND:play()
    end
  end
end

function lutro.joystickreleased(n, b)
  if #SHIPS > n and not SHIPS[n+1].dead then
    local obj = SHIPS[n+1]
    if b == 4 then obj.aup = false
    elseif b == 5 then obj.adown = false
    elseif b == 6 then obj.aleft = false
    elseif b == 7 then obj.aright = false end
  end
end

function object_move(obj, dt)
    obj.x, obj.y = obj.x+(obj.dx*dt), obj.y+(obj.dy*dt)
end

function object_wrap_in(obj)
  if obj.x < LEFT-obj.w then obj.x = RIGHT
  elseif obj.x > RIGHT then obj.x = LEFT-obj.w end
  if obj.y < TOP-obj.h then obj.y = BOTTOM
  elseif obj.y > BOTTOM then obj.y = TOP-obj.h end
end

function reset(obj) obj.x, obj.y = math.random(LEFT, RIGHT)-obj.w, TOP-obj.h end

function is_outside(obj2, obj1, m)
  return not (obj1.x+obj1.w > obj2.x+m and obj1.x+m < obj2.x+obj2.w
          and obj1.y+obj1.h > obj2.y+m and obj1.y+m < obj2.y+obj2.h)
end

function is_inside(obj2, obj1)
  if obj1 and obj2 then
    return obj2.x > obj1.x and obj2.x+obj2.w < obj1.x+obj1.w
       and obj2.y > obj1.y and obj2.y+obj2.h < obj1.y+obj1.h
    end
end

function is_inside_box(x, y, left, top, right, bottom)
  return x > left and y > top and x < right and y < bottom
end

function new_coin()
  local x = math.random(LEFT, RIGHT-32)
  local y = math.random(2) == 1 and BOTTOM or -32
  local coin = new_obj("coin01.png", x, y, 10, 10)
  coin.dx, coin.dy = lutro.math.random(-20, 20), y > 0 and -24 or 24
  return coin
end

function new_obj(filename, x, y, w, h)
  local image = lutro.graphics.newImage(filename)
  local iw, ih = image:getDimensions()
  return {i = image, q = lutro.graphics.newQuad(0, 0, w, h, iw, ih),
          x = x, y = y, w = w, h = h, dx = 0, dy = 0}
end

function new_rock(bottom)
  local x, y = math.random(LEFT, RIGHT-32), bottom and BOTTOM or -32
  local rock = new_obj("rock01.png", x, y, 31, 31)
  rock.dx, rock.dy = lutro.math.random(-20, 20), y > 0 and -24 or 24
  return rock
end

function obj_accelerate(obj, dt)
  local tx, ty = 0, 0
  local max = 750
  if obj.aup and obj.dy > -max then ty = ty-obj.aup end
  if obj.adown and obj.dy < max then ty = ty+obj.adown end
  if obj.aleft and obj.dx > -max then tx = tx-obj.aleft end
  if obj.aright and obj.dx < max then tx = tx+obj.aright end
  obj.dx, obj.dy = obj.dx+(tx*dt), obj.dy+(ty*dt)
  return tx, ty
end

function obj_decelerate(obj, dt)
  obj.dx, obj.dy = obj.dx-((obj.dx/2)*dt), obj.dy-((obj.dy/2)*dt)
end

function obj_draw(obj) lutro.graphics.draw(obj.i, obj.q, obj.x, obj.y) end

